/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package es.deusto.welive.image_uploader;

import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONObject;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by AritzBi on 16/5/16.
 */
public class AuthChecker {
    public static String checkToken(String token){
        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target("https://dev.welive.eu/dev/api/aac/basicprofile/me");
        Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON_TYPE);
        builder.header("Authorization", token);
        Response response = builder.get();
        if (response.getStatus() != 200) {
            return "-1";
        }else{
            JSONObject json_user = new JSONObject(response.readEntity(String.class));
            return  (String) json_user.get("userId");
        }
    }
}
