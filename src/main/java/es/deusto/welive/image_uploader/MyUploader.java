/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package es.deusto.welive.image_uploader;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.photos.*;
import com.flickr4java.flickr.photosets.Photoset;
import com.flickr4java.flickr.photosets.PhotosetsInterface;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.flickr4java.flickr.uploader.Uploader;
import com.flickr4java.flickr.util.AuthStore;
import com.flickr4java.flickr.util.FileAuthStore;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.xml.sax.SAXException;

import java.io.*;
import java.util.*;

/**
 * Created by AritzBi on 11/5/16.
 */
public class MyUploader {
    private static final Logger logger = Logger.getLogger(MyUploader.class);
    private String nsid;
    private String username;
    private String apiKey;
    private String sharedSecret;
    private String token;
    private Flickr flickr;
    private String auth_path;
    //private AuthStore authStore;
    //public static String authsDirStr = System.getProperty("user.home") + File.separatorChar + ".flickrAuth";

    public MyUploader( ) throws FlickrException {
        //File authsDir = new File(authsDirStr);
        //File authsDir = getResourceAsFile("./142950774@N04.auth");
        Properties properties = new Properties();
        InputStream in = null;
        try {
            in = MyUploader.class.getResourceAsStream("/setup.properties");
            if (in != null) {
                properties.load(in);
                this.apiKey = properties.getProperty("apiKey");
                this.sharedSecret = properties.getProperty("secret");
                this.nsid = properties.getProperty("nsid");
                this.username = properties.getProperty("username");
                this.token = properties.getProperty("token");
                this.auth_path = properties.getProperty("auth");
                if (apiKey != null && sharedSecret != null){
                    System.out.println("Found setup.properties in classpath and set apiKey and shared secret");
                    flickr = new Flickr(apiKey, sharedSecret, new REST());
                    /**if (authsDir != null) {
                        this.authStore = new FileAuthStore(authsDir);
                    }**/
                }
                else{
                    System.out.println("Unable to locate setup.properties");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    /**private void authorize() throws IOException, SAXException, FlickrException {
        AuthInterface authInterface = flickr.getAuthInterface();
        Token accessToken = authInterface.getRequestToken();

        String url = authInterface.getAuthorizationUrl(accessToken, Permission.DELETE);
        System.out.println("Follow this URL to authorise yourself on Flickr");
        System.out.println(url);
        System.out.println("Paste in the token it gives you:");
        System.out.print(">>");

        Scanner scanner = new Scanner(System.in);
        String tokenKey = scanner.nextLine();
        System.out.println(tokenKey);
        Token requestToken = authInterface.getAccessToken(accessToken, new Verifier(tokenKey));
        //Token requestToken = new Token(this.token,this.sharedSecret);
        Auth auth = authInterface.checkToken(requestToken);
        System.out.println("astasda");
        RequestContext.getRequestContext().setAuth(auth);
        this.authStore.store(auth);
        scanner.close();
        System.out.println("Thanks.  You probably will not have to do this every time. Auth saved for user: " + auth.getUser().getUsername() + " nsid is: "
                + auth.getUser().getId());
        System.out.println(" AuthToken: " + auth.getToken() + " tokenSecret: " + auth.getTokenSecret());
    }**/

    public static Auth getResourceAsFile(String resourcePath) {
        try {
            InputStream in = MyUploader.class.getResourceAsStream(resourcePath);
            if (in == null) {
                return null;
            }
            File tempFile = File.createTempFile(String.valueOf(in.hashCode()), ".tmp");
            tempFile.deleteOnExit();

            try (FileOutputStream out = new FileOutputStream(tempFile)) {
                //copy stream
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = in.read(buffer)) != -1) {
                    out.write(buffer, 0, bytesRead);
                }
            }

            ObjectInputStream authStream = new ObjectInputStream(new FileInputStream(tempFile));
            Auth authInst = null;
            try {
                authInst = (Auth) authStream.readObject();
                return authInst;
            } catch (ClassCastException cce) {
                // ignore. Its not an auth, so we won't store it. simple as that :-);
            } catch (ClassNotFoundException e) {
                // yep, ignoring. LALALALALLALAL. I can't hear you :-)
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String uploadfileWithDetails(String user_id, InputStream image, String filename, float size, Date mod_date ) throws Exception {
        String photoId = "";
        String suffix;
        String title = "";
        UploadMetaData metaData = new UploadMetaData();
        RequestContext rc = RequestContext.getRequestContext();
        /**if (this.authStore != null) {
            Auth auth = this.authStore.retrieve(this.nsid);
            if (auth == null) {
                this.authorize();
            } else {
                rc.setAuth(auth);
            }
        }**/
        Auth auth = getResourceAsFile(auth_path);
        rc.setAuth(auth);
        boolean setMimeType = true; // change during testing. Doesn't seem to be supported at this time in flickr.
        if (setMimeType) {
            if (filename.lastIndexOf('.') > 0) {
                title = filename.substring(0, filename.lastIndexOf('.'));
                suffix = filename.substring(filename.lastIndexOf('.') + 1);
                if (suffix.equalsIgnoreCase("png")) {
                    metaData.setFilemimetype("image/png");
                } else if (suffix.equalsIgnoreCase("mpg") || suffix.equalsIgnoreCase("mpeg")) {
                    metaData.setFilemimetype("video/mpeg");
                } else if (suffix.equalsIgnoreCase("mov")) {
                    metaData.setFilemimetype("video/quicktime");
                }
            }
        }
        Uploader uploader = flickr.getUploader();
        try {
            metaData.setFilename(filename);
            metaData.setTitle(title);
            photoId = uploader.upload(image, metaData);
            PhotosetsInterface photosetsInt = flickr.getPhotosetsInterface();
            Collection<Photoset> photosets = photosetsInt.getList(this.nsid).getPhotosets();
            String photoset_id = "-1";
            for (Photoset photoset : photosets) {
                if (photoset.getTitle().equals(user_id)){
                    photoset_id = photoset.getId();
                    break;
                }
            }
            if (!photoset_id.equals("-1")){
                photosetsInt.addPhoto(photoset_id,photoId);
            }else{
                photosetsInt.create(user_id,user_id+" album", photoId);
            }
        } finally {

        }

        return (photoId);
    }
    /**public String uploadfileBytes(String filename) throws Exception {
        String photoId = "";
        String title = "";
        String suffix = "";
        UploadMetaData metaData = new UploadMetaData();
        RequestContext rc = RequestContext.getRequestContext();

        if (this.authStore != null) {
            Auth auth = this.authStore.retrieve(this.nsid);
            if (auth == null) {
                this.authorize();
            } else {
                rc.setAuth(auth);
            }
        }
        boolean setMimeType = true; // change during testing. Doesn't seem to be supported at this time in flickr.
        if (setMimeType) {
            if (filename.lastIndexOf('.') > 0) {
                title = filename.substring(0, filename.lastIndexOf('.'));
                suffix = filename.substring(filename.lastIndexOf('.') + 1);
                if (suffix.equalsIgnoreCase("png")) {
                    metaData.setFilemimetype("image/png");
                } else if (suffix.equalsIgnoreCase("mpg") || suffix.equalsIgnoreCase("mpeg")) {
                    metaData.setFilemimetype("video/mpeg");
                } else if (suffix.equalsIgnoreCase("mov")) {
                    metaData.setFilemimetype("video/quicktime");
                }
            }
        }
        logger.debug(" File : " + filename);
        metaData.setTitle(title);
        metaData.setPublicFlag(true);
        Uploader uploader = flickr.getUploader();
        try {
            metaData.setFilename(filename);
            File f = new File(filename);
            photoId = uploader.upload(f, metaData);
            logger.debug(" File : " + filename + " uploaded: photoId = " + photoId);
        } finally {

        }

        return (photoId);
    }**/

    public BufferedInputStream doBackup(String photoID) throws Exception {
        RequestContext rc = RequestContext.getRequestContext();

        /**if (this.authStore != null) {
            Auth auth = this.authStore.retrieve(this.nsid);
            if (auth == null) {
                this.authorize();
            } else {
                rc.setAuth(auth);
            }
        }**/
        Auth auth = getResourceAsFile(auth_path);
        rc.setAuth(auth);

        PhotosInterface photoInt = flickr.getPhotosInterface();
        try{
            Photo p = photoInt.getInfo( photoID,this.sharedSecret);
            BufferedInputStream inStream = new BufferedInputStream(photoInt.getImageAsStream(p, Size.LARGE));
            return inStream;
        }catch (com.flickr4java.flickr.FlickrException e){
            return null;
        }

    }
    public Photo getPhoto(String photoID) throws Exception{
        RequestContext rc = RequestContext.getRequestContext();
        /**if (this.authStore != null) {
            Auth auth = this.authStore.retrieve(this.nsid);
            if (auth == null) {
                this.authorize();
            } else {
                rc.setAuth(auth);
            }
        }**/
        Auth auth = getResourceAsFile(auth_path);
        rc.setAuth(auth);
        PhotosInterface photoInt = flickr.getPhotosInterface();
        try{
            Photo p = photoInt.getInfo( photoID,this.sharedSecret);
            BufferedInputStream inStream = new BufferedInputStream(photoInt.getImageAsStream(p, Size.LARGE));
            JSONObject json = new JSONObject();
            json.put("description", p.getDescription());
            json.put("name", p.getTitle());
            return p;
        }catch (com.flickr4java.flickr.FlickrException e){
            return null;
        }
    }
    public int setTags(TagsObject tags, String user_id) throws SAXException, IOException, FlickrException {
        RequestContext rc = RequestContext.getRequestContext();
        /**if (this.authStore != null) {
            Auth auth = this.authStore.retrieve(this.nsid);
            if (auth == null) {
                this.authorize();
            } else {
                rc.setAuth(auth);
            }
        }**/
        Auth auth = getResourceAsFile(auth_path);
        rc.setAuth(auth);
        int found = -1;
        PhotosInterface photoInt = flickr.getPhotosInterface();
        try{
            Photo p = photoInt.getInfo(tags.getPhoto_id(),this.sharedSecret);
            PhotoAllContext contexts = photoInt.getAllContexts(tags.getPhoto_id());
            List<PhotoSet> photoSets = contexts.getPhotoSetList();
            for( com.flickr4java.flickr.photos.PhotoSet photoset : photoSets){
                if (photoset.getTitle().equals(user_id)){
                    found = 1;
                    break;
                }
            }
            if (found == -1){
                found = -2;
            }
            photoInt.setTags(tags.getPhoto_id(), tags.getTags());
            photoInt.setMeta(tags.getPhoto_id(),p.getTitle(),tags.getDescription());
        }catch (com.flickr4java.flickr.FlickrException e){
        }
        return found;
    }
}
