/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package es.deusto.welive.image_uploader;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import java.util.*;

import java.io.*;

import com.flickr4java.flickr.util.AuthStore;
import com.flickr4java.flickr.util.FileAuthStore;
import org.scribe.model.Token;
import org.scribe.model.Verifier;

/**
 * Created by AritzBi on 16/5/16.
 */
public class GenerateToken {
    public static String authsDirStr = System.getProperty("user.home") + File.separatorChar + "flickrAuth";
    public static void main(String[] args) throws IOException, FlickrException {
        InputStream in = MyUploader.class.getResourceAsStream("/setup.properties");
        Properties properties = new Properties();
        if (in != null) {
            properties.load(in);
            String apiKey = properties.getProperty("apiKey");
            String sharedSecret = properties.getProperty("secret");
            if (apiKey != null && sharedSecret != null) {
                System.out.println("Found setup.properties in classpath and set apiKey and shared secret");
                Flickr flickr = new Flickr(apiKey, sharedSecret, new REST());
                AuthInterface authInterface = flickr.getAuthInterface();
                Token accessToken = authInterface.getRequestToken();
                String url = authInterface.getAuthorizationUrl(accessToken, Permission.DELETE);
                System.out.println("Follow this URL to authorise yourself on Flickr");
                System.out.println(url);
                System.out.println("Paste in the token it gives you:");
                System.out.print(">>");
                Scanner scanner = new Scanner(System.in);
                String tokenKey = scanner.nextLine();
                System.out.println(tokenKey);
                Token requestToken = authInterface.getAccessToken(accessToken, new Verifier(tokenKey));
                Auth auth = authInterface.checkToken(requestToken);
                RequestContext.getRequestContext().setAuth(auth);
                File authsDir = new File(authsDirStr);
                AuthStore authStore = new FileAuthStore(authsDir);
                authStore.store(auth);
                scanner.close();
                System.out.println("Thanks.  You probably will not have to do this every time. Auth saved for user: " + auth.getUser().getUsername() + " nsid is: "
                        + auth.getUser().getId());
                System.out.println(" AuthToken: " + auth.getToken() + " tokenSecret: " + auth.getTokenSecret());
            } else {
                System.out.println("Unable to locate setup.properties");
            }
        }
    }
}
