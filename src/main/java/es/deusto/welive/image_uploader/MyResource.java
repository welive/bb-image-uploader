/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package es.deusto.welive.image_uploader;

import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.tags.Tag;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.*;
import java.util.ArrayList;


@Path("")
@Api(value="/image", description = "Operations about images")
public class MyResource {

    @Context
    private UriInfo uriInfo;
    @POST
    @Path("upload")
    @ApiOperation(value = "Uploads an image and its basic metadata: name and size.")
    @Produces("application/json")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(@HeaderParam("Authorization") String token, @FormDataParam("file") InputStream uploadedInputStream, @FormDataParam("file") FormDataContentDisposition fileDetail) throws Exception {
        String user_id = AuthChecker.checkToken(token);
        System.out.println("Token:" + user_id);
        if (user_id.equals("-1")){
            JSONObject json = new JSONObject();
            json.put("status", "401");
            json.put("message", "Unauthorized token");
            return Response.status(401).entity(json.toString()).build();
        }else{
            //byte [] byteArray = getBytes(uploadedInputStream,fileDetail.getSize());
            MyUploader bf = new MyUploader();
            String photo_id = bf.uploadfileWithDetails(user_id, uploadedInputStream,fileDetail.getFileName(),fileDetail.getSize(),fileDetail.getModificationDate());
            JSONObject json = new JSONObject();
            json.put("status", "200");
            json.put("message", "File uploaded successfully");
            json.put("photo_id", photo_id);
            return Response.ok(json.toString(), MediaType.APPLICATION_JSON).build();
        }

    }

    private String writeToFileServer(InputStream inputStream) throws IOException {

        OutputStream outputStream = null;
        String qualifiedUploadFilePath = "/Users/Aritzbi/subido.jpg";

        try {
            outputStream = new FileOutputStream(new File(qualifiedUploadFilePath));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            outputStream.flush();
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
        finally{
            //release resource, if any
            outputStream.close();
        }
        return qualifiedUploadFilePath;
    }

    private byte[] getBytes(InputStream input, long size) throws IOException {
        byte[] buffer = new byte[(int) size];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
        return output.toByteArray();
    }

    @GET
    @Path("raw_image/{photo_id}")
    @ApiOperation(value = "Returns the raw photo related to provided ID without any metadata")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("image/png")
    public Response downloadFile(@PathParam("photo_id") String photo_id) throws Exception {
        MyUploader bu = new MyUploader();
        BufferedInputStream image = bu.doBackup(photo_id);
        if (image != null )
            return Response.ok(image).build();
        else{
            JSONObject json = new JSONObject();
            json.put("status", "404");
            json.put("message", "Incorrect photo id, image not found");
            return Response.status(404).entity(json.toString()).build();
        }
    }

    @GET
    @Path("image_info/{photo_id}")
    @ApiOperation(value = "Provides metadata about given photograph")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response downloadFile2(@PathParam("photo_id") String photo_id) throws Exception {
         MyUploader bu = new MyUploader();
        Photo p = bu.getPhoto(photo_id);
        if (p != null ){
            JSONObject json = new JSONObject();
            json.put("description", p.getDescription());
            json.put("name", p.getTitle());
            String full_url = uriInfo.getBaseUri() + "raw_image/" + p.getId();
            json.put("url", full_url);
            ArrayList<String> tags_values = new ArrayList<String>();
            for (Tag tag : p.getTags()){
                tags_values.add(tag.getValue());
            }
            json.put("tags", tags_values);
            return Response.ok(json.toString(), MediaType.APPLICATION_JSON).build();
        }
        else{
            JSONObject json = new JSONObject();
            json.put("status", "404");
            json.put("message", "Incorrect photo id, image not found");
            return Response.status(404).entity(json.toString()).build();
        }
    }

    @POST
    @Path("set_metadata")
    @ApiOperation(value = "Associates a set of tags to a photograph")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setTags(@HeaderParam("Authorization") String token, TagsObject tags) throws Exception {
        String user_id = AuthChecker.checkToken(token);
        int found;
        if (!user_id.equals("-1")){
            MyUploader bu = new MyUploader();
            found = bu.setTags(tags, user_id);
        }else{
            found = -2;
        }
        if (found == 1){
            JSONObject json = new JSONObject();
            json.put("status", "200");
            json.put("message", "Image has been successfully updated");
            return Response.ok(json.toString(), MediaType.APPLICATION_JSON).build();
        }else{
            if (found == -1) {
                JSONObject json = new JSONObject();
                json.put("status", "404");
                json.put("message", "Incorrect photo id, image not found");
                return Response.status(404).entity(json.toString()).build();
            }else{
                JSONObject json = new JSONObject();
                json.put("status", "401");
                json.put("message", "Incorrect token or the image does not belong to the provided user");
                return Response.status(401).entity(json.toString()).build();
            }

        }
    }
}
