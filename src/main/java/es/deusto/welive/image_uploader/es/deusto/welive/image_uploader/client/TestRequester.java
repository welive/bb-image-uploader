/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package es.deusto.welive.image_uploader.es.deusto.welive.image_uploader.client;


import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.json.JSONObject;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by AritzBi on 10/5/16.
 */
public class TestRequester {
    private static final String TARGET_URL = "http://imageuploader.welive.smartcommunitylab.it/webapi/upload";
    private static final String TARGET_URL_DOWNLOAD = "http://localhost:8090/test/webapi/myresource/upload";
    public static void main(String[] args) throws FileNotFoundException {
        //TestRequester.setTags();
        TestRequester.upload();
    }

    public static void savePhoto(String id, String directory){
        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);

        WebTarget target = client.target(TARGET_URL_DOWNLOAD);

        String data = "{'photo_id': " + id+ "}";

        Entity json = Entity.entity(data, MediaType.APPLICATION_JSON_TYPE);
        Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON_TYPE);


    }
    public static void upload(){
        Client client = ClientBuilder.newBuilder()
                .register(MultiPartFeature.class).build();
        WebTarget webTarget = client.target(TARGET_URL);
        MultiPart multiPart = new MultiPart();
        FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("file",
                new File("/Users/Aritzbi/doctor_sw.jpg"),MediaType.APPLICATION_OCTET_STREAM_TYPE);
        multiPart.bodyPart(fileDataBodyPart);
        Response response = webTarget.request(
        ).header("Authorization", "Bearer").post(
                Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA));

        System.out.println(response.getStatus()+" "+response.getStatusInfo()+" "+response);
        System.out.println(response.readEntity(String.class));
    }
    public static void setTags(){
        String[] tags = new String[2];
        tags[0] = "Card";
        tags[1] = "cardsahdas";
        JSONObject json = new JSONObject();
        json.put("tags",tags );
        json.put("photo_id", "26922297181" );
        json.put("description", "Photograph's description");
        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target("http://localhost:8080/test/webapi/myresource/set_metadata");
        Entity json_data = Entity.entity(json.toString(), MediaType.APPLICATION_JSON_TYPE);
        Response  response = target.request().header("Authorization", "Bearer ").post(json_data);
        System.out.println(response.getStatus()+" "+response.getStatusInfo()+" "+response);
        System.out.println(response.readEntity(String.class));
    }
}
